#include <iostream>
#include <cstdlib>
#include <cstring>
#include <vector>
#include <string>
#include <algorithm>
using namespace std ;

int int_count(int n)
{
    int number = 0;

do {
     ++number;
     n /= 10;
} while (n);
return number ;
}

class BigDecimalInt {
private:
    vector <int> arr ;
public:
    BigDecimalInt () ;
    BigDecimalInt (string num) ;
    BigDecimalInt (int x) ;
    friend ostream& operator << (ostream& out, BigDecimalInt b) ;
    BigDecimalInt operator + (const BigDecimalInt &right) ;
    BigDecimalInt operator = (const BigDecimalInt &right) ;


//    friend ostream& operator = (ostream& out, BigDecimalInt b) ;
};

BigDecimalInt :: BigDecimalInt () {}

BigDecimalInt :: BigDecimalInt (int x)
{

int s = int_count(x) ;

vector <int> tmp(s) ;

for(int i=s-1; i>=0 ; i--)
{
    tmp[i] = x % 10;
    x /= 10;
}

arr = tmp ;

}

BigDecimalInt :: BigDecimalInt (string num)
{
     for (int i = 0 ; i< num.length() ; i++)
     {
        string tmp = num.substr (i, 1) ;
        arr.push_back( atoi( tmp.c_str() ) ) ;
     }
}


BigDecimalInt BigDecimalInt :: operator + (const BigDecimalInt &right)
{
    int carry = 0, digit, s1, s2 ; vector <int> arr1, arr2, T ; BigDecimalInt tmp ;


    if (arr.size() <=(right.arr).size())
    {
        arr1 = (right.arr) ;
        s1 = arr1.size() ;
        arr2 = arr ;
        s2 = arr2.size() ;

    }

    else
    {
    arr1 = arr ;
    s1 = arr1.size() ;
    arr2 = (right.arr) ;
    s2 = arr2.size() ;

    }

    for (int i = s1-1 ; i>= 0 ; i--)
    {
            if (i-(s1-s2) < 0 )
                    digit = arr1[i] + carry ;

            else digit = arr1[i] + arr2[i-(s1-s2)] + carry ;

            carry = 0 ;
            if (digit>=10 && i !=0)
               {
                    carry++ ;
                    digit -=10 ;
               }

            if  (i != 0)
            (T).push_back(digit) ;
            else
            {
            reverse((T).begin(), (T).end()) ;
            (tmp.arr).push_back(digit) ;
            }
    }
    for (int i = 0 ; i<T.size() ; i++)
    (tmp.arr).push_back(T[i]) ;

   return tmp ;

}

ostream& operator << (ostream& out, BigDecimalInt b) {
     for ( int i = 0 ; i< (b.arr).size() ; i++ )
        out << b.arr[i] ;
    return out ;
}

BigDecimalInt BigDecimalInt:: operator = (const BigDecimalInt &right)
{
    for (int i = 0 ; i< arr.size() ; i++)
        arr.pop_back() ;

    for (int i = 0 ; i< (right.arr).size() ; i++)
        arr.push_back(right.arr[i]) ;
}

int main ()
{
BigDecimalInt num1("123456789012345678901234567890");
BigDecimalInt num2("113456789011345678901134567890");
BigDecimalInt num3 = num2 + num1;
cout << "num1 = " << num1 << endl;
cout << "num2 = " << num2 << endl;
//236913578023691357802369135780
cout << "num2 + num1 = " << num3 << endl;

return 0 ;
}
