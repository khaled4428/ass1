#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <cstring>

using namespace std;

class desired_currency {
private:
    float dollars, amount, ex_rate ; string temp ; ifstream input ;

public:
    desired_currency () ;                // here I work in column D
    desired_currency (int choice) ;
    ~ desired_currency () ;
    void set_dollars (float n) ;
    float get_ex_rate () ;
    float  get_amount () ;
};

desired_currency::desired_currency ()
{
    dollars = amount = ex_rate = 1 ;
}

desired_currency::desired_currency (int choice)
{
    dollars = amount = ex_rate = 1 ;

    input.open("Data.csv", ios::in) ;

    // getting the exchange rate from the file

    for (int i = 1 ; i<choice ;i++)
    {
        getline(input, temp, '\n') ;
        if (i==(choice-1))
    {
            for (int j = 1 ; j<=4 ; j++)
        {
                getline(input, temp, ',') ;

        }
    }
    }
ex_rate = atof(temp.c_str()) ;

 }

void desired_currency :: set_dollars (float n)
{
    dollars = n ;
}

float desired_currency :: get_ex_rate ()
{
    return ex_rate ;
}

float desired_currency ::  get_amount ()
{
    amount = ex_rate * dollars ;
    return amount ;
}

desired_currency :: ~ desired_currency ()
{
    input.close() ;
}

class myCurrency {
private:
    float in_dollars, myCur, ex_rate ; string temp ; ifstream input ;


public:
    myCurrency () ;                // here I work in column C
    myCurrency (int choice) ;
    ~ myCurrency () ;
    float get_ex_rate () ;
    float  get_inDollars () ;
    void  set_myCur (float n) ;

};

myCurrency::myCurrency ()
{
    in_dollars = myCur = ex_rate = 1 ;
}

myCurrency::myCurrency (int choice)
 {
    in_dollars = myCur = ex_rate = 1 ;

    ifstream input ;
    input.open("Data.csv", ios::in) ;

    // getting the exchange rate from the file

    for (int i = 1 ; i<choice ;i++)
    {
        getline(input, temp, '\n') ;
        if (i==(choice-1))
    {
            for (int j = 1 ; j<=3 ; j++)
        {
                getline(input, temp, ',') ;

        }
    }
    }
ex_rate = atof(temp.c_str()) ;

 }

void myCurrency :: set_myCur (float n)
{
    myCur = n ;
}

float myCurrency :: get_ex_rate ()
{
    return ex_rate ;
}

float myCurrency :: get_inDollars ()
{
    in_dollars = ex_rate * myCur ;
    return in_dollars ;
}

myCurrency :: ~ myCurrency ()
{
    input.close() ;
}

int main()
{
    string selection, num, name, unit ; ifstream outfile ; int choice1, choice2 ; float amount ; char d ;

    cout << "You will see 196 currencies to exchange among one another. \n \nPlease enter the number shown beside to choose it. \n\n" ;
    system("PAUSE") ;
    system("CLS") ;
do {
    outfile.open("Data.csv", ios::in) ;

    for (int i = 1 ; i<=196 ; i++)
    {
        getline(outfile, num, ',') ; // read the number
        getline (outfile, name, ',') ; // read the currency name

        selection = num + "- " + name ;

        cout<<selection<<endl ;

        getline(outfile, selection, '\n') ; //move the cursor to the end of the row ;

    }

    outfile.close() ;

    cout<<endl<<"Enter the currency number you want to exchange to: " ; cin>>choice1 ;

    system("CLS") ;

    outfile.open("Data.csv", ios::in) ;

    for (int i = 1 ; i<=196 ; i++)
    {
        getline(outfile, num, ',') ; // read the number
        getline (outfile, name, ',') ; // read the currency name
        selection = num + "- " + name ;

        cout<<selection<<endl ;

        getline(outfile, selection, '\n') ; //move the cursor to the end of the row ;

         if (choice1==atoi(num.c_str()))              //record the name of the currency to use it as a unit
            unit = name ;
    }
    outfile.close() ;

    cout<<endl<<"Enter the currency number you want to exchange from: " ; cin>>choice2 ;

    system("CLS") ;

    cout<< "Enter the amount of money: " ; cin>>amount ;

    myCurrency a(choice2) ;          //from your currency to dollars
    a.set_myCur(amount) ;
    amount = a.get_inDollars() ;

    desired_currency b(choice1) ;    //from dollars to the chosen currency
    b.set_dollars(amount) ;
    amount = b.get_amount() ;

    system("CLS") ;
    cout<<"The amount is: " << amount <<" " <<unit<<"\n\n"<<"want another conversion? [e -> exit] or [c -> continue]\n\n" ; cin>>d ;
    if (d=='e' || d=='E')
        break ;
} while(true) ;
return 0;
}
